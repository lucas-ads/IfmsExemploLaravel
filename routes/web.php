<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sobre',function(){
    return view('sobre');
});

Route::get('/contato',function(){
    return view('contato');
});

Route::get('/welcome/{nome?}',function($name="Estudante"){
  return "Bem-vindo $name!";
})->where('nome','[a-zA-Z]+');
